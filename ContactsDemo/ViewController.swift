//
//  ViewController.swift
//  ContactsDemo
//
//  Created by Petr on 07.01.18.
//  Copyright © 2018 Andi. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    let cellId = "cellId"
    
    let names = ["Andi", "Peter", "Lukas", "Asterix", "Franz", "Jasper", "Angelina", "Torsten", "Ingrid", "Leonard"]
    
    let anotherListofNames = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Contacts"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // register cell class with reuse identifier
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = "Header"
        label.backgroundColor = UIColor.lightGray
        return label
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return names.count
        }
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let name = names[indexPath.row]
        cell.textLabel?.text = name
        
        cell.textLabel?.text = name + " Section: " + String(indexPath.section) + " Row: " + String(indexPath.row)
        
        return cell
    }

}
